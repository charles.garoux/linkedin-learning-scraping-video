// ==UserScript==
// @name LinkedIn Learning video scraper
// @version 0.2
// @author Charles Garoux
// @description Get video links on LinkedIn learning and store the video's datas on filesystem.
// @match https://www.linkedin.com/learning/*
// @exclude https://www.linkedin.com/learning/me
// @run-at context-menu
// ==/UserScript==

console.log("Debut du script de scraping");

var secondToWait = 1;

function wait(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
}

// if (!confirm('On lance tous ça? :)')) {
// 	alert("Rien ne sera fait sans ton accord :)");
// 	return ;
// }
// alert("C'est partie! ;)");

var listLiensNodeList = document.querySelectorAll('[data-control-name="course_video_route"]');

// console.log(listLiensNodeList);

for (var i = 0; i < 4 /*listLiensNodeList.length*/; i++) {
	console.log(listLiensNodeList[i].href);
	var link = listLiensNodeList[i].href;
	link = link.replace("?autoplay=true", "");
	link = link + "?key=" + i;
    wait(secondToWait * 1000);
    console.log(i);
    window.open(link,'_blank');

}

console.log("Fin du script de scraping");
