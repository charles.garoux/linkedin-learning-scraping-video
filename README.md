# Linkedin Learning scraping video

**Projet personnel**

Actuellement, Linkedin propose seulement la lecture des formations hors-connexion seulement sur mobile. J'ai recherche à créer une solution me permettant de le faire sur mon ordinateur.

## Objectif

Découvrir le JavaScript.

Pouvoir avoir les vidéos sur mon ordinateur lisible sans connexion internet.

## Résultat

Un script en **JavaScript** s’exécute sur le navigateur avec Tampermonkey, une extension de navigateur qui exécute du Javascript, et un serveur HTTP tourne sûr l'ordinateur pour exécuter du **PHP**.

Le script JS envoie des requête AJAX vers le serveur et le PHP généré un script **BASH** qu'il faudra exécuter pour télécharger les vidéos.

> PS: De nombreuses autres fonctionnalités peuvent être ajoutées. A ce stade, le script ne fonctionne que sur la version découverte de Linkedin Learning.