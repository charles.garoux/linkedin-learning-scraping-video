// ==UserScript==
// @name 101 - Leveling planner
// @namespace https://openuserjs.org/scripts/metazeta/101_-_Leveling_planner
// @version 0.4
// @author Emmanuel Ruaud and Benjamin Fuhro
// @description Estimates total XP on user profiles and gives the amount won by past projects
// @match https://profile.intra.42.fr/users/*
// @match https://profile.intra.42.fr/
// @match https://profile.intra.42.fr/users/*/experiences/*/*
// @updateURL https://openuserjs.org/meta/metazeta/101_-_Leveling_planner.meta.js
// @license MIT
// ==/UserScript==

function get_max_xp(lvl) {
  return (100 * Math.pow(1.15, lvl + 1));
}

function get_delta_xp(split1, split2) {
    let diff_lvl = 0;
    let i;
    for (i = Number(split1[0]); i <= Number(split2[0]); i++) { 
        diff_lvl += get_max_xp(i);
    }
    let prev_lvl = Number(split1[1]) * get_max_xp(Number(split1[0])) / 100;
    let next_lvl = (100 - Number(split2[1])) * get_max_xp(Number(split2[0])) / 100;
    console.log(split1, split2);
    console.log(diff_lvl, next_lvl, prev_lvl);
    return (diff_lvl - next_lvl - prev_lvl);
}

function get_project() {
  let progress = document.getElementsByClassName("on-progress");
  let lvlstr = progress[0].innerText || progress[0].textContent;
  let fraction = Number(parseFloat(lvlstr.match(/\d+\%/)[0]));
  let lvl = Number(parseFloat(lvlstr.match(/\d+ -/)[0]));

  let maxlvl = get_max_xp(Number(lvl));
  let text = document.createTextNode("   (" + Math.round(fraction / 100 * maxlvl) + "/" + Math.round(maxlvl) + " XP)");
  progress[0].appendChild(text);
  let projects = document.getElementsByClassName("project-item block-item");
}

function experience() {
  let lvlbox = document.getElementsByTagName("code");
  let reasons = document.getElementsByClassName("reason");
  let lvlstr;
  let split_prev = [0, 0];
  let split;
  let deltaXP;
  let i = lvlbox.length - 1;
  while (lvlbox[i]) {
    lvlstr = lvlbox[i].innerText || lvlbox[i].textContent;
    split = lvlstr.split(".");
    deltaXP = Math.round(get_delta_xp(split_prev, split));
    reasons[i].innerHTML = reasons[i].innerHTML + "<div style='font-size:12px;color:#73BDBC;display:inline-block;left:60%;'> +" + deltaXP + " XP" + "</div>";
    split_prev = split;
    i--;
  }
}

(function () {
  'use strict';
  if ((document.baseURI).match(/^https:\/\/profile\.intra\.42\.fr\/users\/[a-z0-9-_]+$/i) ||
    (document.baseURI).match(/^https:\/\/profile\.intra\.42\.fr\/$/i)
  ) {
    get_project();
  }
  else {
    experience();
  }
})();