#! /usr/bin/php
<?php
// exemple URL https://files3.lynda.com/secure/courses/5016729/VBR_MP4h264_main_SD/5016729_00_01_WX30_Welcome.mp4?Aoq4J3ZY953PPDZIS8VseRGT8pNlIvN3rNMNYzkzrr8McZURwiAzIhJr6KpvQn5iV1MOqkVW4HMLNhDXQtlWR4MddOdOOqP68fjdmW9SBZQTicajcShBfpfcy-A1toZTRzOgur8Qro59FinyiHhNljdxhYup9_27O3GNQc0K1xwcK6ErU5EHhQ

$source = $argv[1];
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $source);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_SSLVERSION,3);
$data = curl_exec ($ch);
$error = curl_error($ch);
curl_close ($ch);

$destination = $argv[2];
$file = fopen($destination, "wb");
fwrite($file, $data);
fclose($file);
?>
