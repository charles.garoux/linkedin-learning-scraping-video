<?php
/* ==========================================================
** Cote serveur du projet "LinkedIn Learning scraping video"
** Version 0.1
**======================================================== */

/*
** Recois une requet HTTP avec en POST :
**	login (identifant de connection de l'utilisateur)
**	password (mot de passe de l'utilisateur)
**	title (titre de la video)
**	course_title (titre du cours)
**	link (lien de telechargement)
**
** Ecrit dans le fichier de script
**
*/

/* =============== Lancement du buffer =============== */
ob_flush();
ob_start();
/* ====== Variable "d'environnement" du script  ====== */
$login = 'toto';
$password = 'secret';
$scriptFile = 'tampermonkey_dl_script.bash';
$logFile = 'tampermonkey_log.txt';
$logHeader = '['. date('d-m-Y H:i:s') .']';
/* ============== Debut du script  ==================== */
echo $logHeader; // log

// if (!file_exists($_POST['course_title']))
//   mkdir($_POST['course_title']);

if ($login == $_POST['login'] || $password == $_POST['password']) {
	echo ' '. $login; // log
	$commandLine = 'COURSE_TITLE="'. $_POST['course_title'] .'"; if [ ! -d "$COURSE_TITLE" ]; then mkdir "$COURSE_TITLE"; fi && VIDEO_PATH="${COURSE_TITLE}/'. $_POST['title'] .'.mp4"; if [ ! -e "$VIDEO_PATH" ]; then curl '. $_POST['link'] .' > "$VIDEO_PATH"; else echo "[Deja existant] $VIDEO_PATH"; fi '. PHP_EOL;
	file_put_contents($scriptFile, $commandLine, FILE_APPEND);
}
else {
	echo ' bad login data'; // log
}

/* ============= Enregistrement des log ecrit dans le buffer ============= */
echo PHP_EOL;
file_put_contents($logFile, ob_get_flush(), FILE_APPEND);
?>
