// ==UserScript==
// @name LinkedIn Learning download
// @version 0.2
// @author Charles Garoux
// @description Get video links on LinkedIn learning and store the video's datas on filesystem.
// @match https://www.linkedin.com/learning/*/*
// @exclude https://www.linkedin.com/learning/me

// @grant        GM_xmlhttpRequest
// @connect localhost
// run-at context-menu

// ==/UserScript==

var login = "login";
var password = "secret";

console.log("Debut du script de téléchargement");
/* ==================== BIBLIOTHEQUE ==================== */
function parseURLParams(url) {
	var queryStart = url.indexOf("?") + 1,
	queryEnd   = url.indexOf("#") + 1 || url.length + 1,
	query = url.slice(queryStart, queryEnd - 1),
	pairs = query.replace(/\+/g, " ").split("&"),
	parms = {}, i, n, v, nv;

	if (query === url || query === "") return;

	for (i = 0; i < pairs.length; i++) {
		nv = pairs[i].split("=", 2);
		n = decodeURIComponent(nv[0]);
		v = decodeURIComponent(nv[1]);

		if (!parms.hasOwnProperty(n)) parms[n] = [];
		parms[n].push(nv.length === 2 ? v : null);
	}
	return parms;
}
/* ==================== LANCER UNE FOIS LE CHARGEMENT FINI ==================== */
window.addEventListener('load', function() {
	var video = document.getElementsByTagName("video");
	var httpGet = parseURLParams(window.location.href);
	console.log(httpGet);//debug
	var videoKey =  "[" + httpGet["key"] + "]";
	console.log(videoKey);//debug


	// console.log(video); //debug
	// console.log("video.lenght=" + video.lenght); //debug
	if (video.length == 0) {
		console.log("Aucune video trouve");
		return ;
	}
	var video_link = video[0].src;

	var course_title =  document.getElementsByClassName("course-banner__meta-title");
	if (course_title.length == 0) {
		console.log("Aucune titre trouve");
		return ;
	}
	course_title = course_title[0].getElementsByTagName('a');
	course_title = course_title[0].textContent;

	var title =  document.getElementsByClassName("course-banner__headline");
	if (title.length == 0) {
		console.log("Aucune titre trouve");
		return ;
	}
	title = title[0].textContent.replace("Vous regardez :", "")
	title = title.trim();

	console.log("Titre trouve = " + title);
	console.log("Titre du cours trouve = " + course_title);
	console.log("Lien trouve = " + video_link);


	var post_datas = "login=" + login;
	post_datas += "&password=" + password;
	post_datas += "&course_title=" + course_title;
	post_datas += "&title=" + videoKey + title;
	post_datas += "&link=" + video_link;

	GM_xmlhttpRequest({
	  method: "POST",
	  url: "http://localhost:8008/index.php",
	  data: post_datas,
	  headers: {
	    "Content-Type": "application/x-www-form-urlencoded"
	  },
	  onload: function(response) {
	    //S'execute une fois la requet envoye
			console.log("Le colis a ete expedier ;)");
			console.log("Fin du script de téléchargement");
	  }
	});
	// prompt("Le lien :", video[0].src);
}, false);
/* ============================================================================ */






/* ==================== CODE MIS DE COTE ==================== */


/*
**	Est-ce que utiliser les notifiacation pourrai etre utile?
*/

// // request permission on page load
// document.addEventListener('DOMContentLoaded', function() {
//  if (!Notification) {
//   alert('Desktop notifications not available in your browser. Try Chromium.');
//   return;
//  }
//
//  if (Notification.permission !== 'granted')
//   Notification.requestPermission();
// });
//
//
// function notifyMe() {
//  if (Notification.permission !== 'granted')
//   Notification.requestPermission();
//  else {
//   var notification = new Notification('Notification title', {
//    icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
//    body: 'Hey there! You\'ve been notified!',
//   });
//   notification.onclick = function() {
//    window.open('http://stackoverflow.com/a/13328397/1269037');
//   };
//  }
// }
//
// notifyMe();
